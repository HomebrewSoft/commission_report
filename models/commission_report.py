from odoo import api, fields, models
import datetime


class CommissionReport(models.TransientModel):
    _name = "commission.report"

    year = fields.Integer()
    week_number = fields.Integer()
    start_date = fields.Date(
        readonly=True,
        compute="_compute_date",
    )
    end_date = fields.Date(
        readonly=True,
        compute="_compute_date",
    )
    date = fields.Date()
    payment_date_commission = fields.Date(
        string="Payment Date",
    )
    res_partner_id = fields.Many2one(
        comodel_name="res.users",
    )
    account_move_id = fields.Many2many(
        comodel_name="account.move",
        compute="_compute_account_move_id",
    )
    count = fields.Integer(
        store=True,
    )
    account_payment_id = fields.Many2many(
        comodel_name="account.payment",
        compute="_compute_payments",
    )
    invoices_related_id = fields.Many2many(
        comodel_name="account.move",
        compute="_compute_invoices_related_id",
    )
    company_id = fields.Many2one(
        comodel_name="res.company",
        string="Company",
        default=lambda self: self.env.company,
    )

    def _compute_date(self):
        self.start_date = datetime.datetime.strptime(
            f"{self.year}-W{int(self.week_number)- 1}-0", "%Y-W%W-%w"
        ).date()
        self.end_date = self.start_date + datetime.timedelta(days=6.9)

    def _compute_payments(self):
        self.account_payment_id = self.env["account.payment"].search(
            [
                ("date", ">=", self.start_date),
                ("date", "<=", self.end_date),
            ]
        )

    def _compute_account_move_id(self):
        self.account_move_id = self.env["account.move"].search(
            [
                ("invoice_date", ">=", self.start_date),
                ("invoice_date", "<=", self.end_date),
            ]
        )
        for payments in self.account_payment_id:
            self.account_move_id |= payments.reconciled_invoice_ids

    def _get_tuple_i_p(self, invoice, payment_dict):
        usd = self.env.ref("base.USD")
        mxn = self.env.ref("base.MXN")
        if payment_dict["account_payment_id"]:
            payment = self.env["account.payment"].search(
                [
                    ("id", "=", payment_dict["account_payment_id"]),
                ]
            )
            tax_pay = (
                1 - (invoice.amount_untaxed / invoice.amount_total)
            ) * payment.amount
            sub_payment = payment.amount - tax_pay
            percentage = payment_dict["amount"] / payment.amount
            pay_amount_total = sub_payment * percentage
            if invoice.currency_id == usd:
                pay_amount_total = usd._convert(
                    pay_amount_total,
                    mxn,
                    invoice.company_id,
                    payment.payment_date,
                )
            return (invoice, pay_amount_total)

    def get_invoices_payments(self):
        tuples_i_p = []
        for invoice in self.account_move_id:
            if not invoice.invoice_user_id == self.res_partner_id:
                continue

            # Call the new method to compute the payment information
            invoice._compute_payments_widget_reconciled_info()

            # Get the payment information from the computed dictionary
            try:
                payments = invoice.invoice_payments_widget['content']
            except:
                continue

            for payment in payments:
                if (
                        payment["account_payment_id"]
                        and invoice.invoice_date >= payment["date"]
                ):
                    if (
                            payment["account_payment_id"]
                            and payment["date"] <= self.end_date
                    ):
                        tuples_i_p.append(self._get_tuple_i_p(invoice, payment))
                else:
                    if (
                            payment["account_payment_id"]
                            and self.start_date <= payment["date"] <= self.end_date
                    ):
                        tuples_i_p.append(self._get_tuple_i_p(invoice, payment))
        return tuples_i_p or []

    def print(self):
        self.count += 1  # TODO fix
        return self.env.ref("commission_report.report_commission").report_action(self)
