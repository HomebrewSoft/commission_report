from odoo import api, fields, models
from datetime import date


class AccountMove(models.Model):
    _inherit = "account.move"

    flete1_id = fields.Many2one(
        comodel_name="account.move",
    )
    flete2_id = fields.Many2one(
        comodel_name="account.move",
    )
    payment_date = fields.Date(
        compute="_compute_account_payment_id",
        store=True,
    )
    payment_date2 = fields.Date()  # TO-DO
    days = fields.Integer(
        compute="_compute_days",
    )
    payment_terms_line_days = fields.Integer(
        compute="_payment_terms_days",
    )
    pay_amount = fields.Monetary()

    @api.depends("invoice_payment_term_id.line_ids")
    def _payment_terms_days(self):
        for record in self:
            days = 0
            for line in record.invoice_payment_term_id.line_ids:
                days = days + line.days
            record.payment_terms_line_days = days

    # @api.depends("invoice_payments_widget")
    # def _compute_account_payment_id(self):
    #     for move in self:
    #         payments = move._get_reconciled_info_JSON_values()
    #         if not payments:
    #             move.payment_date = False
    #             continue
    #         payments = sorted(payments, key=lambda p: p["date"])
    #         last_payment = payments[-1]
    #         move.payment_date = last_payment["date"]
    #

    # New code odoo 16
    # ----------------------------------------------------------
    def get_reconciled_info(self):
        reconciled_info = []
        for line in self.line_ids:
            if line.matched_debit_ids:
                for matched_debit in line.matched_debit_ids:
                    reconciled_info.append({
                        'name': matched_debit.debit_move_id.move_id.name,
                        'journal_name': matched_debit.debit_move_id.move_id.journal_id.name,
                        'amount': matched_debit.amount,
                        'currency': matched_debit.debit_move_id.currency_id.name,
                        'date_maturity': matched_debit.debit_move_id.date_maturity,
                    })
            if line.matched_credit_ids:
                for matched_credit in line.matched_credit_ids:
                    reconciled_info.append({
                        'name': matched_credit.credit_move_id.move_id.name,
                        'journal_name': matched_credit.credit_move_id.move_id.journal_id.name,
                        'amount': matched_credit.amount,
                        'currency': matched_credit.credit_move_id.currency_id.name,
                        'date_maturity': matched_credit.credit_move_id.date_maturity,
                    })
        return reconciled_info

    @api.depends("invoice_payments_widget")
    def _compute_account_payment_id(self):
        for move in self:
            payments = move.get_reconciled_info()
            if not payments:
                move.payment_date = None
                continue
            payments = sorted(payments, key=lambda p: p["date_maturity"] if p["date_maturity"] else date.min)
            last_payment = payments[-1]
            move.payment_date = last_payment["date_maturity"]

    # ----------------------------------------------------------

    def _compute_days(self):
        for record in self:
            if record.invoice_date and record.payment_date:
                start_date = record.invoice_date
                end_date = record.payment_date
                date = end_date - start_date
                record.days = date.days
            else:
                record.days = 0
