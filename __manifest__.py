{
    "name": "Commission Report",
    "version": "16.0.0.2.0",
    "author": "HomebrewSoft",
    "website": "https://homebrewsoft.dev",
    "license": "LGPL-3",
    "depends": [
        "account",
    ],
    "data": [
        # security
        "security/ir.model.access.csv",
        # reports
        "reports/paperformat_commission_reports.xml",
        "reports/commission_report.xml",
        # views
        "views/account_move.xml",
        "views/commission_report.xml",
    ],
}
